import React, {useState, useEffect} from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Container, Row, Col, Button, Form} from "react-bootstrap";
import Api from "./api/Api";
import './App.css';

function hook_get_brands(setData, setListData){
  const b = (new_data) => {
    setData(new_data);
    document.getElementById('select-list0').value=parseInt(new_data.brands[0].id);
    document.getElementById('select-list1').value=parseInt(new_data.brands[1].id);
    setListData([parseInt(new_data.brands[0].id), parseInt(new_data.brands[1].id)]);
  }
  return b;
}
function Brands({brands, my_id, list_select, text}){
  console.log(brands.brands);
  const op = brands.brands.map((elem) => (
    <option key={parseInt(elem.id)} value={parseInt(elem.id)}>{elem.name}</option>
  ));
  return (
      <Form.Group>
        <Form.Label>{text}</Form.Label>
        <Form.Control id={"select-list"+my_id} onChange={(event) => {
          let val = parseInt(event.target.value);
          let new_list_select = list_select[0];
          new_list_select[my_id] = val;
          let new_ = list_select[1];
          new_(new_list_select);
        }} as="select" default={"Выберите..."}>
          {op}
        </Form.Control>
      </Form.Group>
  );
}
function ListsItem({object}){
  return (
        <Row>
          <Col>{object.city}</Col>
          <Col>{object.address}</Col>
          <Col style={{textAlign:'center'}}>{object.rivals.count}</Col>
        </Row>
  );
}
function ListsData({data, curr}){
  let pag = data.slice(50*curr, 50*(curr + 1));
  const ans = pag.map((elem, id) => (
      <ListsItem key={id} object={elem}/>
  ));
  return (
      <Container>
        {ans}
      </Container>
  );
}
function App() {
  const [brands, setData] = useState({"brands":[]});
  const list_select_brands = useState([0, 0]);
  const [cafes, setCafes] = useState([]);
  const [pagination, setPagination] = useState({'curr':0, 'min':0, 'max':0});
  const f = hook_get_brands(setData, list_select_brands[1], setPagination);
  useEffect(()=>{
    Api.getAllBrands(f);
  }, []);
  return (
    <div className="App">
      <section className="settings">
        <Container className="mt-4 mb-4">
          <Row>
            <Col>
              <Brands brands={brands} my_id={0} list_select={list_select_brands} text="Выбрать начальное кафе:"/>
            </Col>
            <Col>
              <Brands brands={brands} my_id={1} list_select={list_select_brands} text="Выбрать вторичное кафе:"/>
            </Col>
          </Row>
          <Row className="justify-content-center">
            <Button onClick={() => {
              let c = new Set(list_select_brands[0]);
              if (list_select_brands[0].length === c.size){
                let [a, b] = list_select_brands[0];
                Api.getRival(a, b, setCafes, setPagination);
                console.log(cafes);
              } else {
                alert("Выберите два различных кафе")
              }
            }} style={{margin: "auto"}} variant="outline-primary">Получить данные</Button>
          </Row>
        </Container>
      </section>
      <section className="view">
        <p style={{textAlign:"center", color:"#777"}}>Результаты выводятся в кругу 2км. В API можно редактировать расстояние</p>
        <p/>
        <Container>
          <Form.Group>
            <label htmlFor="pagination">Пагинация:</label>
            <input onChange={(event) => {
              let val = parseInt(event.target.value);
              setPagination({'min': pagination.min, 'max': pagination.max, 'curr': val});
            }} type="range" className="custom-range" min={pagination.min} max={pagination.max} id="pagination" />
          </Form.Group>
        </Container>
        <Container style={{textAlign:"center",fontWeight:"bold"}}>
          <Row>
            <Col>Город: </Col>
            <Col>Адрес кафе: </Col>
            <Col>Колличество конкурентов:</Col>
          </Row>
        </Container>
        <ListsData curr={pagination.curr} data={cafes}/>
      </section>
    </div>
  );
}

export default App;
