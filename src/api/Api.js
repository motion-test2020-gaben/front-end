const HOST = "http://localhost:8000/";
const APP = "api/v1/cafes/";
const Api = {
    getAllBrands: function (hook) {
        fetch(HOST + APP + "brand/all").then(res => res.json()).then(
            (result) => {
                hook({'brands':result});
            }
        )
    },
    getRival: async function(main, rival, hook, setPagination){
        let ans = await fetch(HOST + APP + "rival/", {
            method: 'post',
            headers: {
                'Accept': 'application/json, text/plain, */*',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(
                {
                    "main": main,
                    "rival": rival,
                    "details":true
                }
            )
        });
        ans = await ans.json();
        if (ans.status === "ok"){
            let max = Math.ceil(ans.cafes.length / 50);
            let min = 0;
            let curr = 0;
            setPagination({'min': min, 'max': max, 'curr': curr});
            hook(ans.cafes);
        }
    }
}

export default Api;